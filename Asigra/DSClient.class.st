"
I am the model for an Asigra DS-Client. I hold all the configuration data, including global configuration info plus a list of Schedules and BackupSets

Sample:
| a |
a := DSClient fromFile: 'D:\Pow\Documents\Dean\Cabot.xml'.
a inspect

"
Class {
	#name : #DSClient,
	#superclass : #Object,
	#instVars : [
		'accountName',
		'accountNumber',
		'clientNumber',
		'keysForward',
		'maxGenerations',
		'osType',
		'compressionType',
		'schedules',
		'backupSets',
		'retentionRules',
		'parentSystem'
	],
	#category : #Asigra
}

{ #category : #'instance creation' }
DSClient class >> fromConfigZip: anXMLDataStream [
	^ self new loadConfigurationXML: anXMLDataStream
]

{ #category : #'instance creation' }
DSClient class >> fromFile: aDSConfigurationFile [
	^ self new loadConfigurationFile: aDSConfigurationFile
]

{ #category : #accessing }
DSClient >> accountName [
	" Answer the name of the account holder for this DS-Client."
	^accountName
]

{ #category : #accessing }
DSClient >> accountName: aString [
	"Set the name of the account holder for this DS-Client."
	accountName := aString
]

{ #category : #accessing }
DSClient >> accountNumber [
	" Answer the Customer Number of the account holder for this DS-Client."
	^accountNumber
]

{ #category : #accessing }
DSClient >> accountNumber: aString [
	"Set the name of the account number for this DS-Client."
	accountNumber := aString
]

{ #category : #'detail information' }
DSClient >> activeSchedules [
	"Answer an OrderedCollection of all DSSchedules defined to this DS-Client that are currently suspended"
	^ (self schedules) select: [ :aSched | aSched isActive ] 
]

{ #category : #'detail information' }
DSClient >> backupsHaveRetention [
	"Answer with an OrderedCollection of all DSBackupsets belonging to this DS-Client that have a retention rule attached to them."
	
	^ self backupsets select: [ :aSet | aSet hasRetentionPolicy ]
]

{ #category : #'detail information' }
DSClient >> backupsHaveSchedule [
	"Answer with an OrderedCollection of all DSBackupsets belonging to this DS-Client that have a  schedule attached to them."
	
	^ self backupsets select: [ :aSet | aSet hasSchedule ]
]

{ #category : #'detail information' }
DSClient >> backupsNoRetention [
	"Answer with an OrderedCollection of all DSBackupsets belonging to this DS-Client that do not have a retention rule attached to them. These will never delete older backups"
	
	^ self backupsets reject: [ :aSet | aSet hasRetentionPolicy ]
]

{ #category : #'detail information' }
DSClient >> backupsNotScheduled [
	"Answer with an OrderedCollection of all DSBackupsets belonging to this DS-Client that have no schedule attached to them. These are defined but will never run"
	
	^ self backupsets reject: [ :aSet | aSet hasSchedule ]
]

{ #category : #'detail information' }
DSClient >> backupsetCount [
	^ self backupsets size
]

{ #category : #'detail information' }
DSClient >> backupsetCountsByType [
	| countTracker counts |
	
	countTracker := Dictionary new.
	self backupsets do: [ :aSet |
		countTracker at: (aSet type)
			ifAbsentPut: 0.
			
		counts := countTracker at: (aSet type).
		countTracker at: (aSet type) put: (counts + 1)
		].
	
	^ countTracker
]

{ #category : #'detail information' }
DSClient >> backupsetTypes [
	^ (self backupsets collect: [ :aSet | aSet type ]) asSet
]

{ #category : #'detail information' }
DSClient >> backupsets [
	"Answer an OrderedCollection of all DSBackupSets defined to this DS-Client"
	^ backupSets 
]

{ #category : #accessing }
DSClient >> clientDC [
	" Answer the Datacenter to which this DSClient connects"
	| out |
	out := 'DC'.
	6 to: 7 do: [ :x |
		| nextChar |
		nextChar := (self clientNumber) at: x.
		out := out, (nextChar asString)].
	
	^ out
]

{ #category : #accessing }
DSClient >> clientNumber [
	" Answer the unique identifing number of this DS-Client."
	^clientNumber
]

{ #category : #accessing }
DSClient >> clientNumber: aString [
	"Set the unique number of this DS-Client."
	clientNumber := aString
]

{ #category : #'detail information' }
DSClient >> compressionDisagreements [
	"Answer the receiver with an OrderedCollection of DSBackupsets whose individual compression algorithm is not the same as the global configuration set in the DSClient"
	^(self backupsets) reject: [ :aSet | (aSet compressionType) asUppercase
			= (self compressionType) asUppercase ].
]

{ #category : #accessing }
DSClient >> compressionType [
	" Answer the default data compression type for this DS-Client."
	^compressionType
]

{ #category : #accessing }
DSClient >> compressionType: aString [
	"Set [LZOP|ZLIB|''] to indicate the default data compression method for this DS-Client."
	
	| tempValue |
	tempValue := aString asUppercase.
	(tempValue = 'ZLIB' or: (tempValue = 'LZOP' or: (tempValue = '') ) ) 
	ifTrue: [
		compressionType := tempValue ].
]

{ #category : #initialization }
DSClient >> initialize [
	schedules := OrderedCollection new.
	backupSets := OrderedCollection new.
	retentionRules := OrderedCollection new.
	^self accountName: '';
		accountNumber: '';
		clientNumber: '';
		keysForward: '';
		compressionType: '';
		osType: '';
		yourself
]

{ #category : #accessing }
DSClient >> isLinuxClient [
	^ (self osType = 'linux')
]

{ #category : #accessing }
DSClient >> isWindowsClient [
	^ (self osType = 'windows')
]

{ #category : #accessing }
DSClient >> keysForward [
	" Answer yes or no to indicate if this DS-Client automatically forwards encryption keys to DS-System."
	^keysForward
]

{ #category : #accessing }
DSClient >> keysForward: aString [
	"Set [yes|no|''] to indicate if this DS-Client automatically forwards encryption keys to DS-System."
	
	| tempValue |
	tempValue := aString asLowercase.
	(tempValue = 'yes' or: (tempValue = 'no' or: (tempValue = '') ) ) 
	ifTrue: [ keysForward := tempValue ].
]

{ #category : #loading }
DSClient >> loadConfigurationFile: anXMLFile [

	| xmlDoc document config scheds os sets retentions |
	xmlDoc := XMLDOMParser parse: (FileStream fileNamed: anXMLFile) readStream.
	os := (xmlDoc doctypeDeclaration) systemID.
	self osType: os.

	document := xmlDoc elementAt: 'client-config'.
	config := document elementAt: 'configuration'.
	scheds := document elementAt: 'schedules'.
	sets := document elementAt: 'backup-sets'.
	retentions := document elementAt: 'retentions'.

	self parseSetupConfig: config;
		parseDefaultsConfig: config;
		parseSchedules: scheds;
		parseBackupsets: sets;
		parseRetentionRules: retentions.
]

{ #category : #loading }
DSClient >> loadConfigurationXML: anXMLStream [

	| xmlDoc document config scheds os sets retentions fixedStream |
	"d := anXMLStream asString."
	fixedStream := (self stripNonUTF8: anXMLStream) asByteArray.
	xmlDoc := [XMLDOMParser parse: fixedStream] on: XMLEncodingException do: [^ nil].
	os := (xmlDoc doctypeDeclaration) systemID.
	os = 'asigra_conf_mobile.dtd' ifTrue: [ ^ nil ].
	self osType: os.

	document := xmlDoc elementAt: 'client-config'.
	config := document elementAt: 'configuration'.
	scheds := document elementAt: 'schedules'.
	sets := document elementAt: 'backup-sets'.
	retentions := document elementAt: 'retentions'.

	self parseSetupConfig: config;
		parseDefaultsConfig: config;
		parseSchedules: scheds;
		parseBackupsets: sets;
		parseRetentionRules: retentions.
	
	^ self
]

{ #category : #accessing }
DSClient >> maxGenerations [
	"Answer the maximum number of online backup generations for this DS-Client."
	^maxGenerations
]

{ #category : #accessing }
DSClient >> maxGenerations: aValue [
	"Set the allowed maximum online generations for this DS-Client."
	
	| maxAsNumber |
	(aValue isKindOf: Integer)
	ifTrue: [ 
		maxGenerations := aValue ]
	ifFalse: [ 
		maxAsNumber := aValue asInteger. 
		maxGenerations := maxAsNumber
		 ]
]

{ #category : #accessing }
DSClient >> osType [
	" Answer the operating system this DS-Client is installed on."
	^osType 
]

{ #category : #accessing }
DSClient >> osType: aString [
	"Based on the XMLDocumentTypeDeclaration, set the operating system this DS-Client is installed on."
	
	| tempValue |
	osType := ''.
	tempValue := aString asLowercase.
	tempValue = 'asigra_conf_windows.dtd'
	ifTrue: [
		osType := 'windows' ].
	
	tempValue = 'asigra_conf_linux.dtd'
	ifTrue: [ 
		osType := 'linux' ].

]

{ #category : #accessing }
DSClient >> parentSystem [
	" Answer the DS-System to which this DS-Client is attached."
	^parentSystem
]

{ #category : #accessing }
DSClient >> parentSystem: aFilePath [
	" Set the DS-System to which this DS-Client is attached."
	| fileElements |
	fileElements := aFilePath substrings: '\'.
	(fileElements at: 1) = '2' ifTrue: [ parentSystem := 'DC02 DS-System N+1 Cluster' ].
	(fileElements at: 1) = '7' ifTrue: [ parentSystem := 'DC04 DS-System N+1 Cluster' ].
	(fileElements at: 1) = '10' ifTrue: [ parentSystem := 'DC01 DS-System N+1 Cluster' ].
	(fileElements at: 1) = '12' ifTrue: [ parentSystem := 'DC04-DSS-S01' ].
	(fileElements at: 1) = '13' ifTrue: [ parentSystem := 'DC04-DSS-S02' ].
	(fileElements at: 1) = '14' ifTrue: [ parentSystem := 'DC01-DSS-S01' ].
	(fileElements at: 1) = '16' ifTrue: [ parentSystem := 'DC02-DSS-S01' ].
	(fileElements at: 1) = '17' ifTrue: [ parentSystem := 'DC01-DSS-S02' ].
	(fileElements at: 1) = '18' ifTrue: [ parentSystem := 'DC02-DSS-S02' ].
	^ parentSystem
]

{ #category : #parsing }
DSClient >> parseBackupsets: aConfigNode [
	| setsNode newBackupNodes |
	aConfigNode ifNil: [ ^ self ].
	newBackupNodes := OrderedCollection new.
	setsNode := aConfigNode elementAt: 'create-backup-sets'.
	setsNode
		allElementsDo: [ :x | 
			(x name endsWith: '-backup-set')
				ifTrue: [ newBackupNodes add: x ] ].
	newBackupNodes
		do: [ :aNode | backupSets add: (DSBackupset loadFromXML: aNode) ].
	^ self
]

{ #category : #parsing }
DSClient >> parseDefaultsConfig: aConfigNode [
	| defaultsCfg online compression |
	aConfigNode ifNil: [ ^ self ].
	
	defaultsCfg := aConfigNode elementAt: 'defaults-config' ifAbsent: [ ^ self ].
	online := defaultsCfg elementAt: 'online-generations'.
	self maxGenerations: (online attributeAt: 'max').
	compression := defaultsCfg elementAt: 'compression'.
	self compressionType: (compression attributeAt: 'type').
	^ self
]

{ #category : #parsing }
DSClient >> parseRetentionRules: aConfigNode [
	| newRule |
	aConfigNode ifNil: [ ^ self ].
	aConfigNode
		elementsDo: [ :thisElement | 
			newRule := DSRetentionRule loadFromXML: thisElement.
			retentionRules add: newRule ].
	^ self
]

{ #category : #parsing }
DSClient >> parseSchedules: aConfigNode [
	| newSched |
	aConfigNode ifNil: [ ^ self ].
	aConfigNode
		elementsDo: [ :schedTag | 
			newSched := DSSchedule from: schedTag.
			schedules add: newSched ].
	^ self
]

{ #category : #parsing }
DSClient >> parseSetupConfig: aConfigNode [
	| setupCfg key anElement |
	aConfigNode ifNil: [ ^ self ].
	setupCfg := aConfigNode elementAt: 'setup-config'.
	setupCfg
		ifNotNil: [ anElement := setupCfg
				elementAt: 'account-name'
				ifAbsent: [ anElement := nil ].
			anElement
				ifNotNil: [ self accountName: (setupCfg elementAt: 'account-name') innerXML ].
			anElement := setupCfg
				elementAt: 'account-number'
				ifAbsent: [ anElement := nil ].
			anElement
				ifNotNil:
					[ self accountNumber: (setupCfg elementAt: 'account-number') innerXML ].
			anElement := setupCfg
				elementAt: 'client-number'
				ifAbsent: [ anElement := nil ].
			anElement
				ifNotNil: [ self clientNumber: (setupCfg elementAt: 'client-number') innerXML ].
			key := setupCfg elementAt: 'keys-forwarding'.
			self keysForward: (key attributeAt: 'enable') ].
	^ self
]

{ #category : #'detail information' }
DSClient >> scheduleCount [
	^ self schedules size
]

{ #category : #'detail information' }
DSClient >> scheduleNames [
	"Answer an OrderedCollection of the names of all Schedules defined in this DS-Client"

	^ (self schedules) collect: [ :aSched | aSched name ].
]

{ #category : #'detail information' }
DSClient >> schedules [
	"Answer an OrderedCollection of all DSSchedules defined to this DS-Client"
	^ schedules 
]

{ #category : #accessing }
DSClient >> stripNonUTF8: aByteArray [
	| byteString |
	byteString := aByteArray asString.
	"a := 'dÇean'."
	^ String streamContents: [ :stream |
		byteString do: [ :eachChar |
			(eachChar codePoint < 127)
				ifTrue: [ stream nextPut: eachChar ] ] ]
]

{ #category : #'detail information' }
DSClient >> suspendedSchedules [
	"Answer an OrderedCollection of all DSSchedules defined to this DS-Client that are currently suspended"
	^ (self schedules) select: [ :aSched | aSched isSuspended ] 
]

{ #category : #'detail information' }
DSClient >> unscheduledBackupsets [
	"Answer the receiver with an OrderedList of all backupsets for this DS-Client whose schedule is blank"
	^(self backupsets) select: [ :aBackup | (aBackup scheduleName) isEmpty ].
]

{ #category : #'detail information' }
DSClient >> writeExtractOn: aNeoCSVStream [
	aNeoCSVStream
		writeField: (self accountNumber);
		writeSeparator;
		writeField: (self accountName);
		writeSeparator;
		writeField: (self clientNumber);
		writeSeparator;
		writeField: (self parentSystem);
		writeEndOfLine.
	
	^ self
]
