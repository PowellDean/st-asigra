"
Please comment me using the following template inspired by Class Responsibility Collaborator (CRC) design:

For the Class part:  State a one line summary. For example, ""I represent a paragraph of text"".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages
---
Load an entire DSSystem from a single xml configuration archive:
| system |
system := DSSystem new loadZipArchive: '/home/dean/Downloads/xml_config.zip'.

inspect the result
---
Run an extract of all DSAccounts and their DSClients and write to a specified CSV file: system writeDSClientsExtract: '/home/dean/Downloads/test.csv'
---

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	dsAccounts:		<Object>
	name:		<Object>


    Implementation Points

The following packages must be installed:
	The default Pharo XML parser: 

		Gofer new
			squeaksource: 'MetacelloRepository';
			package: 'ConfigurationOfXMLSupport';
			load.

		ConfiguationOfXMLSupport load.

NeoCSV (for some report generation):
	Metacello new
		repository: 'github://svenvc/NeoCSV/repository';
		baseline: 'NeoCSV';
		load.
		
Then save the image

"
Class {
	#name : #DSSystem,
	#superclass : #Object,
	#instVars : [
		'name',
		'dsAccounts'
	],
	#category : #Asigra
}

{ #category : #adding }
DSSystem >> addDSAccount: aDSAccount [
	dsAccounts add: aDSAccount
]

{ #category : #accessing }
DSSystem >> atAccountWithNumber: anODICustomerNumber [
	^ (self dsAccounts select: [:anAccount | (anAccount accountNumber) = anODICustomerNumber]) at: 1
]

{ #category : #'detail information' }
DSSystem >> backupsNotScheduled [
	"Answer the receiver with an OrderedCollection of all backups sets belonging to all DS-Clients associated with this DSSystem that do not have a retention rule associated with them"
	| result |
	result := OrderedCollection new.
	self dsAccounts do: [ :anAccount |
		"aClient backupsets."
		result := result,(anAccount backupsNotScheduled)
		].
	
	^ result
]

{ #category : #'detail information' }
DSSystem >> backupsetCount [
	"Answer the receiver with a count of all backups sets belonging to DSClients in this DSSystem"
	| result |
	result := 0.
	self dsAccounts do: [ :anAccount |
		"aClient backupsets."
		result := result + (anAccount backupsets size).
		].
	
	^ result
]

{ #category : #accessing }
DSSystem >> dsAccounts [
	^ dsAccounts
]

{ #category : #searching }
DSSystem >> findDSAccount: anODICustomerNumber [
	^ self dsAccounts select: [ :thisAccount | (thisAccount accountNumber) = anODICustomerNumber ]
]

{ #category : #testing }
DSSystem >> hasAccount: anODICustomerNumber [
	| retVal |
	retVal := false.
	(self dsAccounts select: [ :anAccount |
		(anAccount accountNumber = anODICustomerNumber)
			ifTrue: [ retVal := true ]
			]).
	
	^ retVal
]

{ #category : #initialization }
DSSystem >> initialize [
	dsAccounts := OrderedCollection new.
	^self name: '';
		yourself
]

{ #category : #searching }
DSSystem >> linuxDSClients [ 
	| clientList tempList |
	clientList := OrderedCollection new.
	self dsAccounts do: [ :anAccount |
		tempList := anAccount linuxDSClients.
		clientList := clientList,tempList.
		].
	
	^ clientList
]

{ #category : #loading }
DSSystem >> loadZipArchive: asigraXMLConfigZip [
	| zipArchive fileRef aClient newAccount existingAccount tempAccount zipPath |
	zipArchive := ZipArchive new.
	fileRef := asigraXMLConfigZip asFileReference.
	[ zipArchive readFrom: fileRef fullName.
	zipArchive members
		do: [ :aMember | 
			aClient := DSClient new
				loadConfigurationXML: (zipArchive contentsOf: aMember).
			aClient
				ifNil: [ UIManager default
						alert:
							(''''
								join: {('Error! Unable to parse: ' , aMember fileName , '. Skipping')}) ]
				ifNotNil: [ zipPath := aMember fileName.
					aClient parentSystem: zipPath.
					tempAccount := self findDSAccount: aClient accountNumber.
					tempAccount isEmpty
						ifTrue: [ newAccount := DSAccount new.
							newAccount addDSClient: aClient.
							self addDSAccount: newAccount ]
						ifFalse: [ existingAccount := self atAccountWithNumber: aClient accountNumber.
							existingAccount addDSClient: aClient ] ] ] ]
		ensure: [ zipArchive close ]
]

{ #category : #accessing }
DSSystem >> name [
	^ name
]

{ #category : #accessing }
DSSystem >> name: aString [
	name := aString
]

{ #category : #searching }
DSSystem >> windowsDSClients [ 
	| clientList tempList |
	clientList := OrderedCollection new.
	self dsAccounts do: [ :anAccount |
		tempList := anAccount windowsDSClients.
		clientList := clientList,tempList.
		].
	
	^ clientList
]

{ #category : #searching }
DSSystem >> writeDSClientsExtract: aCSVFile [
	| fileRef csvStream |
	fileRef := aCSVFile asFileReference writeStream.
	csvStream := NeoCSVWriter on: fileRef.
	csvStream initialize.
	csvStream writeHeader: #('AccountNumber' 'AccountName' 'DS-Client Number' 'DS-Client System').

	self dsAccounts do: [ :thisAccount | thisAccount writeExtractOn: csvStream ].
	
	csvStream
		flush;
		close.
]
