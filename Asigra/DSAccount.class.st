"
I describe what an Asigra customer account looks like. An Asigra customer has a unique account number, a unique legal name, and has a list of one or more DS-Clients that are provisioned. Some DS-Clients may be inactive or have never been used, but we have no way of telling that just from the configuration file.


Public API and Key Messages

- message one   
- message two

Here's an example of how you would load a DSAccount from a standard xml_zip file downloaded from a single customer in DS-NOC:

| account |
account := DSAccount fromConfigZip: 'C:\Users\Dean Powell\Downloads\xml_config.zip'.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	accountName:		<ByteString>
	accountNumber:		<ByteString>
	dsClients:		<OrderedCollection> of <DSClients>


    Implementation Points

I require an external XML parser to be installed. The default Pharo package works fine:

Gofer new
	squeaksource: 'MetacelloRepository';
	package: 'ConfigurationOfXMLSupport';
	load.

ConfiguationOfXMLSupport load.

Then save the image
"
Class {
	#name : #DSAccount,
	#superclass : #Object,
	#instVars : [
		'accountNumber',
		'accountName',
		'dsClients'
	],
	#category : #Asigra
}

{ #category : #'instance creation' }
DSAccount class >> fromConfigZip: anAsigraXMLConfig [
	^ self new loadZipArchive: anAsigraXMLConfig
]

{ #category : #accessing }
DSAccount >> accountName [ 
	^ accountName 
]

{ #category : #accessing }
DSAccount >> accountName: aByteString [
	accountName := aByteString
]

{ #category : #accessing }
DSAccount >> accountNumber [ 
	^ accountNumber 
]

{ #category : #accessing }
DSAccount >> accountNumber: aByteString [
	accountNumber := aByteString
]

{ #category : #adding }
DSAccount >> addDSClient: aDSClient [
	| foundClient |
	(self dsClients isEmpty)
		ifTrue: [
			^ self addFirstDSClient: aDSClient
			].
		
	(self accountNumber = (aDSClient accountNumber))
		ifTrue: [
			foundClient := self dsClientWithClientNumber: (aDSClient clientNumber).
			(foundClient isEmpty)
				ifTrue: [
					^ self addFirstDSClient: aDSClient
					]
				ifFalse: [ 
					^self error: 'DSClientNumber is already added for this Account'
					]
			]
		ifFalse: [
			"self error: 'This DS-Client cannot belong to this Account'"
			].

]

{ #category : #adding }
DSAccount >> addFirstDSClient: aDSClient [
	self accountNumber: (aDSClient accountNumber).
	self accountName: (aDSClient accountName). 
	dsClients add: aDSClient.

	^ self
]

{ #category : #'detail information' }
DSAccount >> backupsNotScheduled [
	"Answer the receiver with an OrderedCollection of all backups sets belonging to all DS-Clients associated with this DSAccount that do not have a retention rule associated with them"
	| result |
	result := OrderedCollection new.
	self dsClients do: [ :aClient |
		"aClient backupsets."
		result := result,(aClient backupsNotScheduled)
		].
	
	^ result
]

{ #category : #'detail information' }
DSAccount >> backupsetCountsByType [
	"Answer the receiver with a Dictionary containing all backupset types found in all DSClients and a count of how many backupsets each type covers"
	| resultSet clientSetCounts setCount |
	resultSet := Dictionary new.
	self dsClients do: [ :aClient |
		clientSetCounts := aClient backupsetCountsByType.
		clientSetCounts keysAndValuesDo: [ :aKey :aValue |
			resultSet at: aKey
				ifPresent: [
					setCount := resultSet at: aKey.
					resultSet at: aKey put: (setCount + aValue)
					]
				ifAbsentPut: [resultSet at: aKey put: aValue]
			]
		].
	
	^ resultSet
]

{ #category : #'detail information' }
DSAccount >> backupsets [
	"Answer the receiver with an OrderedCollection of all backups sets belonging to all DS-Clients associated with this DSAccount"
	| result |
	result := OrderedCollection new.
	self dsClients do: [ :aClient |
		"aClient backupsets."
		result := result,(aClient backupsets)
		].
	
	^ result
]

{ #category : #searching }
DSAccount >> dsClientWithClientNumber: aClientNumber [
	^ (dsClients select: [:thisClient | thisClient clientNumber = aClientNumber]).
]

{ #category : #accessing }
DSAccount >> dsClients [
	^ dsClients
]

{ #category : #searching }
DSAccount >> extractOn: aNeoCSVStream [
	^ self
]

{ #category : #initialization }
DSAccount >> initialize [
	dsClients := OrderedCollection new.
	^self accountName: '';
		accountNumber: '';
		yourself
]

{ #category : #searching }
DSAccount >> linuxDSClients [
	"Answer the receiver with an OrderedCollection of all DSClients in this account that are installed on Linux servers"
	^(self dsClients) select: [ :aClient | aClient isLinuxClient ]
]

{ #category : #loading }
DSAccount >> loadZipArchive: asigraXMLConfigZip [
| zipArchive fileRef xmlDoc |
zipArchive := ZipArchive new.
fileRef := asigraXMLConfigZip asFileReference.

[zipArchive readFrom: fileRef fullName.
	(zipArchive members) do: [ :aMember |
		xmlDoc := DSClient fromConfigZip: (zipArchive contentsOf: aMember).
		self addDSClient: xmlDoc
		]]
ensure: [ zipArchive close ].

]

{ #category : #searching }
DSAccount >> windowsDSClients [
	"Answer the receiver with an OrderedCollection of all DSClients in this account that are installed on Windows servers"
	^(self dsClients) select: [ :aClient | aClient isWindowsClient ]
]

{ #category : #searching }
DSAccount >> writeExtractOn: aNeoCSVStream [ 
	self dsClients do: [ :aClient | aClient writeExtractOn: aNeoCSVStream ].
]
