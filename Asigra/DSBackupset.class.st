"
I describe what an Asigra BackupSet looks like. I contain Information about the type of set, the full name, and the source server (i.e. the name of the server the backup data is coming from.)
  
Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	name:		<ByteString>
	scheduleName:		<ByteString>
	sourceServer:		<ByteString>
	type:		<ByteString>
	retentionRule:  <ByteString>
	compressionType: <ByteString>
	localStorage: <ByteString> - Allowable values 'yes' or 'no'


    Implementation Points
"
Class {
	#name : #DSBackupset,
	#superclass : #Object,
	#instVars : [
		'type',
		'name',
		'sourceServer',
		'scheduleName',
		'retentionRule',
		'compressionType',
		'localStorage',
		'excludeList',
		'itemsList'
	],
	#category : #Asigra
}

{ #category : #creation }
DSBackupset class >> loadFromXML: anXMLElement [
	^ self new fromXMLElement: anXMLElement.
]

{ #category : #accessing }
DSBackupset >> compressionType [
	^ compressionType 
]

{ #category : #accessing }
DSBackupset >> compressionType: aString [
	compressionType := aString
]

{ #category : #accessing }
DSBackupset >> excludeList [
	"Answer the receiver with an OrderedCollection of filesystem folders to be excluded from this backupset"
	^ excludeList 
]

{ #category : #'instance creation' }
DSBackupset >> fromXMLElement: anXMLElement [
	| setInfoElement sourceElement genericElement aValue |
	self type: anXMLElement name.
	(anXMLElement includesElement: 'basic-set-info')
		ifTrue: [ "Looks like a Windows DS-Client"
			setInfoElement := anXMLElement elementAt: 'basic-set-info'.
			aValue := setInfoElement attributeAt: 'name'.
			aValue ifNil: [ aValue := '' ].
			self name: aValue.
			(anXMLElement includesElement: 'file-system-items')
				ifTrue: [ self
						parseWindowsFileSystemItems: (anXMLElement elementAt: 'file-system-items') ] ]
		ifFalse: [ "We must be parsing a Linux DS-Client"
			self name: (anXMLElement attributeAt: 'name').
			(anXMLElement includesElement: 'backup-items')
				ifTrue: [ self
						parseLinuxFileSystemItems: (anXMLElement elementAt: 'backup-items') ] ].
	sourceElement := anXMLElement elementAt: 'source-server'.
	self sourceServer: (sourceElement elementAt: 'server-name') innerXML.
	genericElement := anXMLElement elementAt: 'generic-backup-options'.
	self scheduleName: (genericElement attributeAt: 'schedule-name').
	self retentionRule: (genericElement attributeAt: 'retention-name').
	self
		compressionType: (genericElement attributeAt: 'compression-type').
	self localStorage: (genericElement attributeAt: 'local-storage').
	^ self
]

{ #category : #testing }
DSBackupset >> hasRetentionPolicy [
	^ (self retentionRule isNotEmpty)
]

{ #category : #testing }
DSBackupset >> hasSchedule [
	^ (self scheduleName isNotEmpty)
]

{ #category : #initialization }
DSBackupset >> initialize [
	excludeList := OrderedCollection new.
	itemsList := OrderedCollection new.
	^ self type: '';
		name: '';
		sourceServer: '';
		scheduleName: '';
		retentionRule: '';
		compressionType: '';
		localStorage: '';
	yourself
]

{ #category : #accessing }
DSBackupset >> localStorage [
	^ localStorage 
]

{ #category : #accessing }
DSBackupset >> localStorage: aString [
	localStorage := aString
]

{ #category : #accessing }
DSBackupset >> name [
	^ name
]

{ #category : #accessing }
DSBackupset >> name: aString [
	name := aString
]

{ #category : #parsing }
DSBackupset >> parseLinuxFileSystemItems: anXMLNode [
	| path share fullPath |
	anXMLNode allElementsDo: [ :thisElement |
		(thisElement name = 'directory-exclusion')
			ifTrue: [
				path := thisElement attributeAt: 'path'.
				share := thisElement attributeAt: 'share-name'.
				(share = '/') ifTrue: [ share := '' ].
				fullPath := share,path.
				excludeList add: fullPath.
				].
			
		(thisElement name = 'item')
			ifTrue: [
				path := thisElement attributeAt: 'path'.
				share := thisElement attributeAt: 'share-name'.
				(share = '/') ifTrue: [ share := '' ].
				fullPath := share,path.
				itemsList add: fullPath.
				]
		].
	^ self 
]

{ #category : #parsing }
DSBackupset >> parseWindowsFileSystemItems: anXMLNode [
	| includeSubdirs path share fullPath |
	anXMLNode allElementsDo: [ :thisElement |
		(thisElement name = 'directory-exclusion')
			ifTrue: [
				includeSubdirs := thisElement attributeAt: 'include-sub-dirs'.
				path := thisElement attributeAt: 'path'.
				share := thisElement attributeAt: 'share-name'.
				fullPath := share,path.
				includeSubdirs = 'yes' ifTrue: [ fullPath := fullPath,'\*' ].
				excludeList add: fullPath.
				].
			
		(thisElement name = 'file-system-item')
			ifTrue: [
				includeSubdirs := thisElement attributeAt: 'include-sub-dirs'.
				path := thisElement attributeAt: 'path'.
				share := thisElement attributeAt: 'share-name'.
				fullPath := share,path.
				includeSubdirs = 'yes' ifTrue: [ fullPath := fullPath,'\*' ].
				itemsList add: fullPath.
				]
		].
	^ self 
]

{ #category : #accessing }
DSBackupset >> retentionRule [
	^ retentionRule 
]

{ #category : #accessing }
DSBackupset >> retentionRule: aString [
	retentionRule := aString
]

{ #category : #accessing }
DSBackupset >> scheduleName [
	^ scheduleName
]

{ #category : #accessing }
DSBackupset >> scheduleName: aByteString [
	scheduleName := aByteString 
]

{ #category : #accessing }
DSBackupset >> sourceServer [
	^ sourceServer
]

{ #category : #accessing }
DSBackupset >> sourceServer: aString [
	sourceServer := aString
]

{ #category : #accessing }
DSBackupset >> type [
	^ type
]

{ #category : #accessing }
DSBackupset >> type: aString [
	type := aString
]
