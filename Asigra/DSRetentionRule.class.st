"
I model a Retention rule in Asigra. For now, I only contain the name of the rule, but I may later keep track of other information.

For the Class part:  State a one line summary. For example, ""I represent a paragraph of text"".

For the Responsibility part: Three sentences about my main responsibilities - what I do, what I know.

For the Collaborators Part: State my main collaborators and one line about how I interact with them. 

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	name:		<ByteString>


    Implementation Points
"
Class {
	#name : #DSRetentionRule,
	#superclass : #Object,
	#instVars : [
		'name'
	],
	#category : #Asigra
}

{ #category : #creation }
DSRetentionRule class >> loadFromXML: anXMLElement [
	^ self new fromXMLElement: anXMLElement.
]

{ #category : #'instance creation' }
DSRetentionRule >> fromXMLElement: anXMLElement [
	self name: (anXMLElement attributeAt: 'name').

]

{ #category : #initialization }
DSRetentionRule >> initialize [
	^ self name: '';
	yourself
]

{ #category : #accessing }
DSRetentionRule >> name [
	^ name
]

{ #category : #accessing }
DSRetentionRule >> name: aByteString [
	name := aByteString
]
