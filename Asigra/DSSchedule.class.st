"
I define the model for an Asigra schedule. I don't exist on my own, I exist as part of a DS-Client and am defined only to that client.

Public API and Key Messages

- message one   
- message two 
- (for bonus points) how to create instances.

   One simple example is simply gorgeous.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	name:			<ByteString>
	administrative: 	<ByteString> 	Allowable values yes or no


    Implementation Points
"
Class {
	#name : #DSSchedule,
	#superclass : #Object,
	#instVars : [
		'name',
		'administrative',
		'suspended',
		'runBackup',
		'runRetention'
	],
	#category : #Asigra
}

{ #category : #'instance creation' }
DSSchedule class >> from: aScheduleTag [
	^ self new parseScheduleTag: aScheduleTag.
]

{ #category : #accessing }
DSSchedule >> administrative [
	"Answer yes or no to indicate if this DSSchedule is administrative (defined automatically by the system) or not"
	^administrative
]

{ #category : #accessing }
DSSchedule >> administrative: aString [

| tempValue |
tempValue := aString asLowercase.
(tempValue = 'yes' or: (tempValue = 'no' or: (tempValue = '')))
ifTrue: [ administrative := tempValue ]
]

{ #category : #initialization }
DSSchedule >> initialize [
	^ self name: '';
		administrative: '';
		suspended: '';
	yourself
]

{ #category : #accessing }
DSSchedule >> isActive [
	"Answer true if suspended = 'no'"
	^ (suspended = 'no')
]

{ #category : #accessing }
DSSchedule >> isSuspended [
	"Answer true if suspended = 'yes'"
	^ (suspended = 'yes')
]

{ #category : #accessing }
DSSchedule >> name [
	"Answer the name of this DSSchedule"
	^name
]

{ #category : #accessing }
DSSchedule >> name: aString [
	"Set the name of this DSSchedule"
	name := aString
]

{ #category : #parsing }
DSSchedule >> parseScheduleTag: aScheduleTag [
	| details tasks |
	self name: (aScheduleTag attributeAt: 'name').
	self administrative: (aScheduleTag attributeAt: 'administrative').
	self suspended: (aScheduleTag attributeAt: 'suspended').
	
	details := aScheduleTag elementAt: 'schedule-details'.
	tasks := (details elementAt: 'run-schedule') elementAt: 'schedule-tasks'.
	
	self runBackup: (tasks attributeAt: 'run-backup').
	self runRetention: (tasks attributeAt: 'run-retention').
	^ self
]

{ #category : #accessing }
DSSchedule >> runBackup [
	"Answer yes or no to indicate if this DSSchedule runs a backup or not"
	^runBackup
]

{ #category : #accessing }
DSSchedule >> runBackup: aString [

| tempValue |
tempValue := aString asLowercase.
(tempValue = 'yes' or: (tempValue = 'no' or: (tempValue = '')))
ifTrue: [ runBackup := tempValue ]
]

{ #category : #accessing }
DSSchedule >> runRetention [
	"Answer yes or no to indicate if this DSSchedule runs retention or not"
	^runRetention
]

{ #category : #accessing }
DSSchedule >> runRetention: aString [

| tempValue |
tempValue := aString asLowercase.
(tempValue = 'yes' or: (tempValue = 'no' or: (tempValue = '')))
ifTrue: [ runRetention := tempValue ]
]

{ #category : #accessing }
DSSchedule >> suspended [
	"Answer yes or no to indicate if this DSSchedule is suspended or not"
	^suspended
]

{ #category : #accessing }
DSSchedule >> suspended: aString [

| tempValue |
tempValue := aString asLowercase.
(tempValue = 'yes' or: (tempValue = 'no' or: (tempValue = '')))
ifTrue: [ suspended := tempValue ]
]
